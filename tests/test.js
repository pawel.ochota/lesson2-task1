const sayHello = require('..');

describe('sayHello', () => {
  it('should returns a string', async () => {
    const result = sayHello();
    const result2 = sayHello('test');

    expect(typeof result).toBe('string');
    expect(typeof result2).toBe('string');
  });

  it('should returns "Hello world" when nothing is passed to parameter', async () => {
    const result = sayHello();

    expect(result).toBe('Hello world');
  });

  it('should returns concatenation of "Hello " and parameter', async () => {
    expect(sayHello('Tomasz')).toBe('Hello Tomasz');
    expect(sayHello(undefined)).toBe('Hello world');
    expect(sayHello(null)).toBe('Hello null');
    expect(sayHello(false)).toBe('Hello false');
    expect(sayHello(0)).toBe('Hello 0');
  });
});
